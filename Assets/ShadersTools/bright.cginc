#ifndef BRIGHT_ENHANCER
#define BRIGHT_ENHANCER

#include "UnityCG.cginc"

namespace BrightEnhancer
{
    //Function to limit the bright multiplier (it must be between 0[dark] and 1[brighter])
    fixed clamp_to_max_mult(fixed4 color, fixed mult)
    {
        fixed maxC = max(color.r,max(color.g,color.b));
        if(mult <0.0) return 0;
        if(mult > 1.0) return 1.0/maxC;
        return mult/maxC;
    }

    fixed clamp_to_lum_additive(fixed4 color, fixed add)
    {
        fixed maxC = max(color.r,max(color.g,color.b));
        fixed minC = min(color.r,min(color.g,color.b));
        if(add > 0)
        {
            return add*maxC;
        }
        if(add < 0)
        {
            return add*minC;
        }
        return 0;
    }

    //Function to bright all the fragment of color of an object
    fixed4 brighten_contrast_pixel(fixed4 col, fixed brigth_factor)
    {
        fixed mult = clamp_to_max_mult(col,brigth_factor);
        for(int i1 =0; i1< 3; i1++)
        {
            col[i1] *= mult;
        }
        return col;
    }

    fixed4 brighten_luminosity_pixel(fixed4 col, fixed lum_factor)
    {
        fixed add = clamp_to_lum_additive(col,lum_factor);
        for(int i1=0; i1<3; i1++)
        {
            col[i1] += add;
        }
        return col;
    }
}

#endif
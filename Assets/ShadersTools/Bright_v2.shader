Shader "Unlit/Bright_v2"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Color", Color) = (255,255,0,255)
        [MaterialToggle] _Choose("Texture on ?", Float) = 0
        _Bright("Constrast Modifier", Range(0.0, 1.0)) = 1.0
        _Lum("Luminosity Modifier", Range(-1.0,1.0)) = 0.0
        [MaterialToggle] _DoContrast("Modify Contrast ?", Float) = 0
        [MaterialToggle] _DoLum("Modify Luminosity ?", Float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100
        ZWRITE Off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Assets/ShadersTools/bright.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _Color;
            fixed _Bright;
            fixed _Lum;
            bool _DoContrast;
            bool _DoLum;
            bool _Choose;
            sampler2D _CameraDepthTexture;
            
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col;
                // sample the texture
                if(_Choose)
                {
                    col = tex2D(_MainTex, i.uv);
                }else
                {
                    col = _Color;   
                }
                 

                //Brighten it
                if(_DoContrast)
                {
                    if(_Choose)
                    {
                        col = saturate(lerp(fixed4(0.5,0.5,0.5,1),col,_Bright));
                    }else
                    {
                        col = BrightEnhancer::brighten_contrast_pixel(col,_Bright);
                    }
                }
                else if(_DoLum)
                {
                    col = BrightEnhancer::brighten_luminosity_pixel(col,_Lum);
                }

                //Display it
                return col;
            }
            ENDCG
        }
    }
}

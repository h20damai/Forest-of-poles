using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Windows;
using File = System.IO.File;

namespace DataImport
{
    public class PolePositionImport : MonoBehaviour
    {
        ///summary
        ///This class import a .csv file containing all the data concerning poles position and height in plate basis.
        ///Template csv (a line = a pole)
        /// poleID, x, z, height
        /// 0, 1.2, 2, 2.5
        ///

        [SerializeField] private Vector2 plateDim; // That's the plate dimension on X and Z axis (in cm) 
        [SerializeField] private string poleDataFilepath; //Path to the .csv file containing pole positions data

        [SerializeField] private GameObject polePrefab;

        private void Start()
        {
            LoadPoles();
        }

        #region Poles Loading
        /// <summary>
        /// Retrieve data from the CSV file and place the poles on the virtual scene (with the correct prefab)
        /// </summary>
        private void LoadPoles()
        {
            var data = ScrapCsvData();
            foreach (var poleListData in data)
            {
                var platePos = new Vector3(poleListData[1], poleListData[2], poleListData[3]);
                PlacePole(poleListData[0],PlateToLocalSpace(platePos));
            }
        }

        /// <summary>
        /// Place a pole giving it's x,z coordinate and height.
        /// </summary>
        /// <param name="polData">Vector3 containing (x,height,z) data (in this order)</param>
        private void PlacePole(float id, Vector3 poleData)
        {
            var poleObject = Instantiate(polePrefab, poleData, Quaternion.identity,transform);
            var poleDim = polePrefab.transform.localScale.x;
            poleObject.transform.localScale = new Vector3(poleDim, poleData.y, poleDim);
            poleObject.transform.name = polePrefab.transform.name + id;
        }

        /// <summary>
        /// Convert the plate basis position (below left corner origin) to the unity basis (origin at the center)
        /// </summary>
        /// <param name="platePos"></param>
        /// <returns></returns>
        private Vector3 PlateToLocalSpace(Vector3 platePos)
        {
            return platePos - new Vector3(plateDim.x/100, 0, plateDim.y/100); //It is only a translation (we don't have defined the origin at the same place)
        }
        
        /// <summary> 
        /// Use the ReadCsv class to parse the .csv file containing pole data.
        /// </summary>
        /// <returns> a list containing the poles info (as a list of float)</returns>
        private List<List<float>> ScrapCsvData()
        {
            var csvParser = new Tools.ReadCsvFloat(poleDataFilepath, ",", true);
            return csvParser.GetParseCsv();
        }
        #endregion

    

    }
}

using System;
using UnityEngine;

namespace ShadersRelated
{
    public class LinearBrightening : MonoBehaviour
    {
        [SerializeField] private GameObject camera;
        [SerializeField] private bool applyLinearModification = false;

        public const float MAXAugmentation = 0.8f;
        public const float MINAugmentation = 0.2f;

        [Tooltip("Put here two gameobject that represent the limit where the sphere could be placed.")]
        [SerializeField] private GameObject[] extremaObjects;

        private Material _material;
        private static readonly int Bright = Shader.PropertyToID("_Bright");
        private static readonly int Lum = Shader.PropertyToID("_Lum");

        private void Awake()
        {
            _material = GetComponent<Renderer>().sharedMaterial;
        }

        /// <summary>
        /// Compute the linearized distance from the camera with two extrema. These extrema are set by the experiment.
        /// </summary>
        /// <param name="closest"> Closest point for the camera</param>
        /// <param name="furthest"> Furthest point from the camera</param>
        /// <returns>Values between 0 and 1.</returns>
        private float GetLinearizedDistanceFromCamera(Vector3 closest, Vector3 furthest)
        {
            var camPosition = camera.transform.position;
            var distance = Vector3.Distance(camPosition,transform.position);
            var closestD = Vector3.Distance(camPosition, closest);
            var furthestD = Vector3.Distance(camPosition, furthest);
            return (distance - closestD) / (furthestD - closestD); //Between 0 and 1
        }

        private void SetShaderSpec()
        {
            var spec = Mathf.Lerp(MAXAugmentation, MINAugmentation,
                GetLinearizedDistanceFromCamera(extremaObjects[0].transform.position, extremaObjects[1].transform.position));
            //We modify both (lum & contrast) since only one could be active at a time.
            _material.SetFloat(Bright, spec);
            _material.SetFloat(Lum, spec);
        }

        private void Update()
        {
            if(applyLinearModification) SetShaderSpec();
        }
    }
}

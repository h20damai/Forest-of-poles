﻿using System;
using System.IO;
using UnityEngine;

namespace Tools
{
    public static class JsonParser<T>
    {
        public static T ParseFile(string filepath)
        {
            var jsonString = File.ReadAllText(GetPath() + "/" + filepath);
            return JsonUtility.FromJson<T>(jsonString);
        }

        public static void ExportToJson(T data, string directory, string filename)
        {
            //Convert the object to a structured json
            var jsonString = JsonUtility.ToJson(data);
            
            //Open the file to write to (try at least)
            try
            {
                Debug.Log("The path on which the file is written is " + GetPathRight() +directory + "/" + filename);
                var file = File.CreateText(GetPathRight() +directory + "/" + filename);
                file.Write(jsonString);
                file.Close(); //Close the file
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }

        private static string GetPath()
        {
            var path = GetPathRight();
            return path.Substring(0, path.Length - "/Assets".Length);
        }
        
        public static string GetPathRight()
        {
            #if UNITY_EDITOR
            return Application.dataPath;
            #elif UNITY_ANDROID
            return Application.persistentDataPath;// +fileName;
            #else
            return Application.dataPath;// +"/"+ fileName;
            #endif      
        }
    }
}
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Tools
{
    public class ReadCsvFloat 
    {
        /// <summary>
        /// Parse a CSV file into a List of List<T> (all data must have the same type)
        /// </summary>
        private readonly string _filepath;
        private readonly string _separator;
        private readonly bool _isTheFirstLineHeader;
        private readonly StreamReader _streamReader;

        private readonly List<List<float>> _data; 

        public ReadCsvFloat(string filepath, string separator, bool differentFirstLine)
        {
            this._filepath = filepath;
            this._separator = separator;
            this._isTheFirstLineHeader = differentFirstLine; //if true, we read the first line differently, otherwise not
            _streamReader = new StreamReader(_filepath); //Init the StreamReader
            _data = new List<List<float>>();
        }

        private void ParseFile()
        {
            var firstLine = _isTheFirstLineHeader; //If false, the if condition below is never read.
            while (true)
            {
                var line = _streamReader.ReadLine();
                if (line is null) break;  //Leave the for loop when no line is readable

                var lineValues = line.Split(_separator); //Separate values
                
                if (firstLine) //Deal with first line if needed
                {
                    var header = lineValues.Aggregate("", (current, value) => current + ("," + value));
                    Debug.Log("The current loaded csv has the following column " + header[Range.StartAt(1)]);
                    firstLine = false;
                    continue;
                }
                
                var lineList = lineValues.Select(valueString => float.Parse(valueString, System.Globalization.CultureInfo.InvariantCulture)).ToList();
                _data.Add(lineList); //Add them to the data holder
                
            }
            _streamReader.Close();
        }

        public List<List<float>> GetParseCsv()
        {
            try
            {
                ParseFile();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
            Debug.Log("The file " + _filepath.Split("\\").Last() + " has been successfully parsed.");
            return _data;
        }
    }
}

﻿using System;
using SequenceLogic;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Class for testing some stuffs.
/// </summary>
public class Test : MonoBehaviour
{
    private IterationData _data;
    private void Start()
    {
        _data = Tools.JsonParser<IterationData>.ParseFile("/Assets/Scripts/DataImport/ExpSettings/test.json");
        var it = _data.iteration[1];
        var val = "posA : " + it.posA + " ; posB : " + it.posB;
        print(val);
    }
}

﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Windows;
using File = System.IO.File;

namespace SequenceLogic
{
    public class SequenceUIManager : MonoBehaviour
    {
        [SerializeField] public TextAsset jsonFile;
        [SerializeField] private GameObject startButton;
        [SerializeField] private GameObject nextButton;
        [SerializeField] private GameObject previousButton;
        
        //We'll probably need to add the controller manager
        
        private SequenceModel _model;

        private void Awake()
        {
            var filename = AssetDatabase.GetAssetPath(jsonFile);
            _model = new SequenceModel(filename); //Init the model
        }


        public void StartIteration()
        {
            _model.Start();
            //add other logic
        }

        public void Next()
        {
            _model.Next();
            //Deal with the interaction details
        }
        
        public void Previous()
        {
            _model.Previous();
            //Deal with the interaction details
        }
        
    }
}
﻿using System;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit;
using UnityEngine;
using UnityEngine.Serialization;

namespace SequenceLogic
{
    [Serializable]
    public struct IterationParameters
    {
       
        public string shaderName;
        public string occlusionMode; //Only indicative for the data analysis. 
        public Vector3 posA;
        public Vector3 posB;

        public IterationParameters(string name,string occMode, Vector3 posA, Vector3 posB)
        {
            shaderName = name;
            this.posA = posA;
            this.posB = posB;
            occlusionMode = occMode;
        }
    }

    [Serializable]
    public struct IterationData
    {
        public List<IterationParameters> iteration;
    }
    
    [Serializable]
    public struct IterationSavedSettings
    {
        public float duration;
        public int iterationIndex;

    }

    [Serializable]
    public class IterationSavedData
    {
        public List<IterationSavedSettings> savedSettings = new List<IterationSavedSettings>();
    }
}
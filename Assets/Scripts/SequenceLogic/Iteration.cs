﻿using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

namespace SequenceLogic
{
    public class Iteration : MonoBehaviour
    {
        #region Singleton Declaration
        public static Iteration Singleton;

        private void Awake()
        {
            if(Singleton!= null && Singleton != this) Destroy(Singleton);

            Singleton = this;
        }
        #endregion

        #region Unity Objects to modify at each iteration
        [SerializeField] private GameObject[] spheres; //Contain all the spheres (different shaders and so on) //They must set invisible at the beginning
        [SerializeField] private string[] shaderNames; //Put the shader names in the same order than in spheres.
        private int _sphereAIndex;
        private int  _sphereBIndex;
        #endregion
        
        private float _iterationDuration; //Estimation à un Time.deltaTime près. It has been tested and provide accurate result.

        

        public void StartIteration(IterationParameters parameters)
        {
            _iterationDuration = Time.time; //init
            //Do things ...
            //Select the correct shader
            var index = GetShaderIndex(parameters.shaderName);
            if (index is null)
            {
                print("<color='red'> Error : the shader chosen has not been found. The iteration hasn't been launched.</color>");
                return;
            }
            SelectShader((int) index);
            //Set the shader properties (in Material) : régler les modes de shaders
            //Position the spheres
            PlaceSpheres(parameters.posA, parameters.posB);
            
        }

        /// <summary>
        /// This function is called whenever an iteration is ended.
        /// It return the registered settings for this specific iteration
        /// </summary>
        /// <returns></returns>
        public IterationSavedSettings EndIteration()
        {
                
            _iterationDuration = Time.time - _iterationDuration; //Compute time
            var savedSet = new IterationSavedSettings();
            savedSet.duration = _iterationDuration;
            //Deal with enb iteration logic
            
            print("End at " + _iterationDuration);
            return savedSet;
        }

        #region Unity Modification functions
        
            private void PlaceSpheres(Vector3 posA, Vector3 posB)
            {
                spheres[_sphereAIndex].transform.position = posA;
                spheres[_sphereBIndex].transform.position = posB;
            }

            /// <summary>
            /// This methods change the value of _sphereA and _sphereB that represent the current spheres used for the iteration
            /// </summary>
            /// <param name="shaderIndex"></param>
            private void SelectShader(int shaderIndex)
            {
                //Reset all spheres.
                foreach (var sphere in spheres)
                {
                    sphere.GetComponent<Renderer>().enabled = false; //We hide all of them
                }
                
                //Give the corresponding index for the spheres
                try
                {
                    if (spheres.Length % 2 != 0)
                        throw new Exception(
                            "<color='red'>Error : not enough spheres added in the editor. It must be even. </color>");
                    if(shaderIndex>=spheres.Length/2) throw new Exception("<color='red'>shaderIndex is out of range.</color>");
                    
                    _sphereAIndex = 2 * shaderIndex;
                    _sphereBIndex = _sphereAIndex + 1;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                
            }

            private int? GetShaderIndex(string shaderName)
            {
                for (var index=0;index < shaderNames.Length; index++)
                {
                    if (shaderName.Equals(shaderNames[index]))
                    {
                        return index;
                    }
                }
                Console.WriteLine("<color ='yellow'> "+shaderName + " hasn't been found in the shaderNames database.</color>");
                return null;
            }

        #endregion


    }

    
}
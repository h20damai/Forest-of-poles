using System;
using System.IO;
using Unity.VisualScripting;
using UnityEngine;

namespace SequenceLogic
{
    public class SequenceModel
    {
        private const string ExportPath = "/Scripts/DataExport";
        
        #region Iteration Related Variables

        private int _itCount; //Amount of iterations in the experiment
        private int _currentIt; //Index that store how many iterations have been run for the moment. It's value is -1 when no iteration has been run yet.
        private readonly Iteration _current; //Reference to the Iteration singleton
        
        private IterationData _seqParams; //The parameters of the experiment (in json order)
        private IterationSavedData _seqExportData; //Store the monitored variables and iteration order of the experiment.

        private int[] _iterationOrder; //A array of index that tell the order of the iteration (must be saved at the end)

        #endregion

        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="jsonFileName"> filename of the json to parse</param>
        public SequenceModel(string jsonFileName)
        {
            GetIterationParameters(jsonFileName);
            _seqExportData = new IterationSavedData(); //Initialize the struct to export at the end of the experiment.
            _currentIt = -1;
            _current = Iteration.Singleton;
        }

        /// <summary>
        /// This method initialize the experiment. It set the iteration index and start the first iteration.
        /// </summary>
        public void Start()
        {
            _currentIt = 0;
            _current.StartIteration(_seqParams.iteration[_currentIt]); //Start the first iteration
        }

        /// <summary>
        /// This method is called when the experiment ends. It store the exported values into a .json file.
        /// </summary>
        private void End()
        {
            //We export the data
            var files = Directory.GetFiles(Tools.JsonParser<int>.GetPathRight() + ExportPath); 
            ExportExperimentSettings("experiment" + files.Length+".json"); //get a proper name for the exported file
        }

        

        #region jSon parsing (in and out)

        /// <summary>
        /// This function scrap the settings for the experiment (from a .json file) and store them in the model.
        /// </summary>
        private void GetIterationParameters(string filename)
        {
            _seqParams = Tools.JsonParser<IterationData>.ParseFile(filename);
            _itCount = _seqParams.iteration.Count; //We get the amount of iterations for this experiment.
        }
        
        private void ExportExperimentSettings(string filename)
        {
            Tools.JsonParser<IterationSavedData>.ExportToJson(_seqExportData,ExportPath,filename);
        }
        #endregion
        
        
        
        
        #region Iterations Change Dealer

        private void ChangeIteration(int iterationIndex)
        {
            // If a iteration is currently on, we end it. 
            if (_currentIt >= 0)
            {
                _seqExportData.savedSettings.Add(_current.EndIteration()); //End iteration and get the exported values.
            }
            
            //Then, depending on the case, we will either : go to the desired iteration or end the experiment (or throw an error if needed)
            try
            {
                if (iterationIndex < 0 || iterationIndex > _itCount) //Out of bounds
                    throw new Exception("<color='red'> iteration index is out of bounds </color>.");
                if (iterationIndex == _itCount) //End of experiment
                    End();
                else //Otherwise, we just go to another iteration.
                {
                    _currentIt = iterationIndex;
                    _current.StartIteration(_seqParams.iteration[_currentIt]);    
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void Next()
        {
            ChangeIteration(_currentIt+1);
        }

        public void Previous() //! Il faut faire attention à ne pas écraser les données en faisant previous (à bien gérer) 
        {
            //TODO : Penser à l'écrasement de fichier : faire attention.
            ChangeIteration(_currentIt-1);
        }
        
        #endregion
    }
    
}
